# TranslatableRepositoryTrait

Gérer la traductibilité d'un repository

``` php
<?php

namespace Models\Traits;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

use Components\Collection\EntityCollection;

/**
 * Class TranslatableRepository
 *
 * This is my translatable repository that offers methods to retrieve results with translations
 */
trait TranslatableRepositoryTrait
{
    /**
     * @var string Default locale
     */
    protected $defaultLocale;


    /**
     * Sets default locale
     *
     * @param string $locale
     */
    public function setDefaultLocale($locale)
    {
        $this->defaultLocale = $locale;
    }

    /**
     * Returns translated one (or null if not found) result for given locale
     *
     * @param QueryBuilder $qb            A Doctrine query builder instance
     * @param string       $locale        A locale name
     * @param string       $hydrationMode A Doctrine results hydration mode
     *
     * @return QueryBuilder
     */
    public function getOneOrNullResult(QueryBuilder $qb, $locale = null, $hydrationMode = null)
    {
        return $this->getTranslatedQuery($qb, $locale)->getOneOrNullResult($hydrationMode);
    }

    /**
     * Returns translated results for given locale
     *
     * @param QueryBuilder $qb            A Doctrine query builder instance
     * @param string       $locale        A locale name
     * @param string       $hydrationMode A Doctrine results hydration mode
     *
     * @return QueryBuilder
     */
    public function getResult(QueryBuilder $qb, $locale = null, $hydrationMode = AbstractQuery::HYDRATE_OBJECT)
    {
        $query = $this->getTranslatedQuery($qb, $locale);
        return new EntityCollection($query->getResult($hydrationMode));
    }

    /**
     * Returns translated array results for given locale
     *
     * @param QueryBuilder $qb     A Doctrine query builder instance
     * @param string       $locale A locale name
     *
     * @return QueryBuilder
     */
    public function getArrayResult(QueryBuilder $qb, $locale = null)
    {
        return $this->getTranslatedQuery($qb, $locale)->getArrayResult();
    }

    /**
     * Returns translated single result for given locale
     *
     * @param QueryBuilder $qb            A Doctrine query builder instance
     * @param string       $locale        A locale name
     * @param string       $hydrationMode A Doctrine results hydration mode
     *
     * @return QueryBuilder
     */
    public function getSingleResult(QueryBuilder $qb, $locale = null, $hydrationMode = null)
    {
        return $this->getTranslatedQuery($qb, $locale)->getSingleResult($hydrationMode);
    }

    /**
     * Returns translated scalar result for given locale
     *
     * @param QueryBuilder $qb     A Doctrine query builder instance
     * @param string       $locale A locale name
     *
     * @return QueryBuilder
     */
    public function getScalarResult(QueryBuilder $qb, $locale = null)
    {
        return $this->getTranslatedQuery($qb, $locale)->getScalarResult();
    }

    /**
     * Returns translated single scalar result for given locale
     *
     * @param QueryBuilder $qb     A Doctrine query builder instance
     * @param string       $locale A locale name
     *
     * @return QueryBuilder
     */
    public function getSingleScalarResult(QueryBuilder $qb, $locale = null)
    {
        return $this->getTranslatedQuery($qb, $locale)->getSingleScalarResult();
    }

    /**
     * Returns translated Doctrine query instance
     *
     * @param QueryBuilder $qb     A Doctrine query builder instance
     * @param string       $locale A locale name
     *
     * @return Query
     */
    protected function getTranslatedQuery(QueryBuilder $qb, $locale = null)
    {
        return $this->getTranlatedQueryBuilder($qb, $locale)->getQuery();
    }

    /**
     * Returns translated Doctrine query builder instance
     *
     * @param QueryBuilder $qb     A Doctrine query builder instance
     * @param string       $locale A locale name
     *
     * @return QueryBuilder
     */
    public function getTranlatedQueryBuilder(QueryBuilder $qb, $locale = null)
    {
        $alias  = $qb->getRootAlias();
        $qb
            ->select($alias, 'trans');
        if ($locale !== false) {
            $locale = (null === $locale) ? $this->defaultLocale : $locale;
            $qb
                ->leftJoin($alias . '.translations', 'trans', 'WITH', 'trans.locale = :locale')
                ->setParameter('locale', $locale);
        } else {
            $qb
                ->leftJoin($alias . '.translations', 'trans', 'WITH');
        }

        return $qb;
    }

    /**
     * Returns all posts
     *
     * @param  array        $criteria
     * @param  array|null   $orderBy
     * @param  integer|null $limit
     * @param  integer|null $offset
     * @param  string|null  $locale
     *
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null, $locale = null)
    {
        $qb = $this->createQueryBuilder('entity');

        if (!empty($orderBy)) {
            foreach ($orderBy as $field => $order) {
                if (empty($field)) {
                    $field = $order;
                    $order = 'ASC';
                }

                if (strpos($field, '.') === false) {
                    $field = $qb->getRootAlias().'.'.$field;
                }

                $qb->addOrderBy($field, $order);
            }
        }

        if (!empty($criteria)) {
            foreach ($criteria as $field => $value) {
                if (strpos($field, '.') === false) {
                    $fullName = $qb->getRootAlias().'.'.$field;
                }

                $qb->andWhere($fullName ." = :".$field)
                    ->setParameter($field, $value);
            }
        }

        if (!empty($limit)) {
            $qb->setMaxResults($limit);
        }

        if (!empty($offset)) {
            $qb->setFirstResult($offset);
        }

        return $this->getResult($qb, $locale);
    }

    /**
     * Find By multiples ids
     *
     * @param  array $ids
     *
     * @return ArrayCollection
     */
    public function findByIds($ids, $locale = null)
    {
        if (is_array($ids) && count($ids) > 0) {
            $qb = $this->createQueryBuilder('a');
            $qb = $this->getTranlatedQueryBuilder($qb, $locale);
            $qb->where($qb->expr()->in('a.id', $ids));

            return $qb->getQuery()->getResult();
        }

        return null;
    }
}
```
