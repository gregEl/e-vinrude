# SpecificQueryBuilderTrait

Permet la gestion de QueryBuilder spécifique

``` php
<?php

namespace App\Traits\Repository;

/**
 * SpecificQueryBuilderTrait
 */
trait SpecificQueryBuilderTrait
{
    /**
     * Get query builder
     *
     * @return QueryBuilder
     */
    abstract protected function getQueryBuilderClass();

    /**
     * Get query builder
     *
     * @return QueryBuilder
     */
    protected function getQueryBuilder($alias)
    {
        $class = $this->getQueryBuilderClass();

        return new $class($this->_em, $alias);
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return $this->getQueryBuilder($alias)
            ->select($alias)
            ->from($this->_entityName, $alias, $indexBy);
    }
}

```
