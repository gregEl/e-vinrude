# PaginatorTrait

Permet la gestion d'un KNP paginator

``` php
<?php

namespace App\Traits\Service\Manager;

use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Paginator;

trait PaginatorTrait
{
    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * Set Paginator
     *
     * @param PaginatorInterface $paginator
     *
     * @return ManagerInterface
     */
    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;

        return $this;
    }

    /**
     * Get Paginator
     *
     * @return PaginatorInterface
     */
    public function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * Get Pagination
     *
     * @param  Query $query
     *
     * @return PaginatedItems
     */
    public function getPagination($query, $page = 1, $limit = null, $options = array())
    {
        return $this->getPaginator()->paginate(
            $query,
            $page,
            $limit,
            $options
        );
    }
}

```
