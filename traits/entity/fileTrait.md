# FileTrait

Gérer un fichier avec gauffrette

``` php
<?php

namespace App\Traits\Entity;

use App\Interfaces\Entity\FileInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * File trait
 *
 * A class using this trait:
 * - MUST implements FileInterface (or DeletableFileInterface)
 * - MUST have a protected/private "$file" variable of class "Symfony\Component\HttpFoundation\File\UploadedFile"
 *   This variable SHOULD have an assertion doc block such as @Assert\Image()
 * - MUST have a "computeFilesystemName" static public function, returning gaufrette filesystem name
 * - MAY have a custom "getUploadDir"
 * - SHOULD use HasSourceTrait
 */
trait FileTrait
{
    /**
     * @var null|string
     *
     * @ORM\Column(name="file_name", type="text", nullable=true)
     */
    protected $fileName;

    /**
     * @var null|string
     *
     * @ORM\Column(name="file_original_name", type="string", length=255, nullable=true)
     */
    protected $fileOriginalName;

    /**
     * @var null|int
     *
     * @ORM\Column(name="file_size", type="bigint", nullable=true)
     */
    protected $fileSize;

    /**
     * @var null|string
     *
     * @ORM\Column(name="file_mime_type", type="string", length=255, nullable=true)
     */
    protected $fileMimeType;

    /**
     * @var null|string
     *
     * @ORM\Column(name="file_checksum", type="string", length=255, nullable=true)
     */
    protected $fileChecksum;

    /**
     * File uploaded date
     *
     * @var null|\DateTime
     *
     * @ORM\Column(name="file_date", type="datetime", nullable=true)
     */
    protected $fileDate;

    /**
     * Set file
     *
     * @param  UploadedFile $file
     * @return self
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        return $this
            ->setFileOriginalName($file->getClientOriginalName())
            ->setFileSize($file->getSize())
            ->setFileMimeType($file->getMimeType())
            ->setFileChecksum(md5_file($file->getPathname()))
            ->setFileDate(new \DateTime())
        ;
    }

    /**
    * Get file
    *
    * @return UploadedFile
    */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Has file
     *
     * @return bool
     */
    public function hasFile()
    {
        return (bool) $this->fileName;
    }

    /**
     * Clean file
     *
     * @return self
     */
    public function cleanFile()
    {
        return $this
            ->setFileName(null)
            ->setFileOriginalName(null)
            ->setFileSize(null)
            ->setFileMimeType(null)
            ->setFileChecksum(null)
            ->setFileDate(null)
        ;
    }

    /**
     * Clone file
     *
     * @return UploadedFile
     */
    public function cloneFile()
    {
        return new UploadedFile(
            'gaufrette://' . $this->getFilePath(),
            basename($this->getFileOriginalName() ? $this->getFileOriginalName() : $this->getFileName())
        );
    }

    /**
     * Set file name
     *
     * @param string $name
     * @return self
     */
    public function setFileName($name)
    {
        $this->fileName = $name;

        return $this;
    }

    /**
     * Get file name
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set file original name
     *
     * @param string $name
     * @return self
     */
    public function setFileOriginalName($name)
    {
        $this->fileOriginalName = $name;

        return $this;
    }

    /**
     * Get file original name
     *
     * @return string
     */
    public function getFileOriginalName()
    {
        return $this->fileOriginalName;
    }

    /**
     * Set file size
     *
     * @param int $size
     * @return self
     */
    public function setFileSize($size)
    {
        $this->fileSize = $size;

        return $this;
    }

    /**
     * Get file size
     *
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * Set file mime type
     *
     * @param string $type
     * @return self
     */
    public function setFileMimeType($type)
    {
        $this->fileMimeType = $type;

        return $this;
    }

    /**
     * Get file mime type
     *
     * @return string
     */
    public function getFileMimeType()
    {
        return $this->fileMimeType;
    }

    /**
     * Set file checksum
     *
     * @param string $checksum
     * @return self
     */
    public function setFileChecksum($checksum)
    {
        $this->fileChecksum = $checksum;

        return $this;
    }

    /**
     * Get file checksum
     *
     * @return string
     */
    public function getFileChecksum()
    {
        return $this->fileChecksum;
    }

    /**
     * Set file date
     *
     * @param null|\DateTime $date
     * @return self
     */
    public function setFileDate(\DateTime $date = null)
    {
        $this->fileDate = $date;

        return $this;
    }

    /**
     * Get file date
     *
     * @return null|\DateTime
     */
    public function getFileDate()
    {
        return $this->fileDate;
    }

    /**
     * Get upload dir
     *
     * @return string|null
     */
    public function getUploadDir()
    {
        return null;
    }

    /**
     * Hash path
     *
     * Given an original path, a path is computed using a md5 hash of this path
     * preceded by a level of subdirectories, based on the hash itself.
     *
     * Example for a "foo" path with a level of 3
     * -> a/c/b/acbd18db4cc2f85cedef654fccc4a4d8
     *
     * @param string $path
     * @param int    $level
     *
     * @return string
     */
    protected function hashPath($path, $level = 0)
    {
        $hash = md5($path);

        if ($level == 0) {
            return $hash;
        } else {
            $hashPath = $hash;
            for ($i = $level; $i >= 1; $i--) {
                $hashPath = $hash[$i - 1] . '/' . $hashPath;
            }

            return $hashPath;
        }
    }

    /**
     * Get filesystem name
     *
     * @return string
     */
    public function getFilesystemName()
    {
        return static::computeFilesystemName($this->getFilesystemNameParameters());
    }

    /**
     * Returns the full file path
     *
     * @return string
     */
    public function getFilePath()
    {
        return static::computeFilePath($this);
    }

    /**
     * Returns the full file path
     *
     * @return string
     */
    public static function computeFilePathByFileName($fileName)
    {
        return $fileName ? static::computeFilesystemName(self::getStaticFilesystemNameParameters()) . '/' . ltrim($fileName, '/') : null;
    }

    /**
     * Compute file path
     *
     * @param FileInterface $file
     *
     * @return string
     */
    public static function computeFilePath(FileInterface $file)
    {
        return $file->getFileName() ? static::computeFilesystemName($file->getFilesystemNameParameters()) . '/' . ltrim($file->getFileName(), '/') : null;
    }

    /**
     * Check if current uploaded file is an image
     *
     * @return boolean
     */
    public function isImage()
    {
        if ($this->getFile() instanceof FileInterface) {
            $type = $this->getFile()->getMimeType();
        } elseif (!empty($this->getFileMimeType())) {
            $type = $this->getFileMimeType();
        } else {
            return false;
        }

        if (strpos($type, 'image/') === 0) {
            return true;
        }
        return false;
    }

    /**
     * Set file from file
     *
     * @param File $file
     *
     * @return self
     */
    public function setFileFromFile($file)
    {
        $this
            ->setFileName($file->getFileName())
            ->setFileOriginalName($file->getFileOriginalName())
            ->setFileSize($file->getFileSize())
            ->setFileMimeType($file->getFileMimeType())
            ->setFileChecksum($file->getFileChecksum())
            ->setFileDate($file->getFileDate())
        ;

        return $this;
    }

    /**
     * Set file from a url
     *
     * @param string $url
     *
     * @return self
     */
    public function setFileFromUrl($url)
    {
        $this
            ->setSource($url)
            ->setFileDate(new \DateTime())
        ;

        return $this;
    }

    /**
     * Create upload file
     *
     * @param  string $path
     * @param  string $filename
     *
     * @return UploadedFile
     */
    public static function createUploadedFile($path, $filename)
    {
        $mineType = mime_content_type($path);
        $size     = filesize($path);

        return new UploadedFile($path, $filename, $mineType, $size, UPLOAD_ERR_OK, true);
    }

    /**
     * Create upload file
     *
     * @param  string $path
     * @param  string $filename
     *
     * @return UploadedFile
     */
    public static function createUploadedFileFromFile($file)
    {
        return self::createUploadedFile($file->getPathname(), $file->getFilename());
    }

    /**
     * Create file
     *
     * @param  string    $content
     *
     * @return SplFileObject
     */
    public static function createFile($content)
    {
        $fileNameTemp = tempnam(sys_get_temp_dir(), 'png');
        $file = new \SplFileObject($fileNameTemp, 'w');
        $file->fwrite($content);
        clearstatcache(false, $file->getPath());

        return $file;
    }
}

```
