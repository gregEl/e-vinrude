# TranslatableEntityTrait

Gérer la traductibilité d'une entité

``` php
<?php

namespace Models\Traits;

use Doctrine\Common\Collections\ArrayCollection;

trait TranslatableEntityTrait
{
    /**
     * The current translation
     * @var array
     */
    protected $currentTranslation;

    public function getCurrentTranslation()
    {
        if (!$this->currentTranslation) {
            $this->setCurrentTranslationByLocale('fr');
        }

        return $this->currentTranslation;
    }

    public function setCurrentTranslation($translation)
    {
        if ($translation) {
            $this->currentTranslation = $translation;
        }

        return $this;
    }

    public function setTranslations($translations)
    {
        $this->translations = $translations;

        return $this;
    }

    public function setCurrentTranslationByLocale($locale)
    {
        return $this->setCurrentTranslation($this->getTranslationByLocale($locale));
    }

    /**
     * See https://github.com/KnpLabs/DoctrineBehaviors#proxy-translations
     *
     * @param string $method
     * @param array $arguments
     * @return type
     */
    public function __call($method, $arguments)
    {
        if (strpos($method, "set") === false) {
            $method = ('get' === substr($method, 0, 3)) ? $method : 'get'. ucfirst($method);

            if (!$translation = $this->getCurrentTranslation()) {
                return;
            }

            if (method_exists($translation, $method)) {
                return call_user_func(array($translation, $method));
            }

            return;
        } else {
            if (!$translation = $this->getCurrentTranslation()) {
                return;
            }
            return $translation->$method($arguments[0]);
        }
    }

    public function hasTranslation($locale)
    {
        $trans = $this->getTranslations();

        return isset($trans[$locale]);
    }


    public function getDefaultTranslation($fallback)
    {
        if ($this->hasTranslation($fallback)) {
            return $fallback;
        } else {
            foreach ($this->getTranslations() as $key => $trans) {
                return $key;
            }
        }

        return null;
    }

    public function filterTranslation($locale, $fallback = 'en')
    {
        $find = true;

        if (!$this->hasTranslation($locale)) {
            $find = false;
            $locale = $this->getDefaultTranslation($fallback);
        }

        foreach ($this->getTranslations() as $key => $trans) {
            if ($locale != $key) {
                $this->removeTranslation($trans);
            }
        }

        return $find;
    }

    /**
     * Get Translation by locale
     *
     * @param  string $locale
     *
     * @return TranslationInterface | null
     */
    public function getTranslationByLocale($locale)
    {
        foreach ($this->getTranslations() as $translation) {
            if ($translation->getLocale() == $locale) {
                return $translation;
            }
        }

        return false;
    }
}
```
