# TimestampableTrait

Gérer le temps sur une entité

``` php
<?php

namespace App\Traits\Entity;

/**
 * Timestampble trait. Provides timestampable feature to an entity.
 */
trait TimestampableTrait
{
    /**
     * @var \DateTime $createdAt createdAt
     */
    protected $createdAt;

    /**
     * @var \DateTime $updatedAt updatedAt
     */
    protected $updatedAt;

    /**
     * Set created at
     *
     * @param \DateTime $createdAt
     *
     * @return TimestampableTrait
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get created at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated at
     *
     * @param \DateTime $updatedAt
     *
     * @return TimestampableTrait
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updated at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Reset created at
     *
     * @return self
     */
    protected function resetCreatedAt()
    {
        return $this
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
        ;
    }

    /**
     * Reset updated at
     *
     * @return self
     */
    public function resetUpdatedAt()
    {
        return $this
            ->setUpdatedAt(new \DateTime())
        ;
    }
}

```
