# AddressTrait

Gérer une adresse dans une entité

``` php

<?php
namespace Models\Traits;

use Doctrine\ORM\Mapping as ORM;
use Models\Request\Address;

trait HasAddressTrait
{
    /**
     * @var Address
     */
    protected $addressObject;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    protected $address;

    /**
     * Additional address
     *
     * @var string
     *
     * @ORM\Column(name="additional_address", type="text", nullable=true)
     */
    protected $additionalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=32, nullable=true)
     */
    protected $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    protected $region;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    protected $country;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    protected $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    protected $longitude;

    /**
     * Set address
     *
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this->updateAddressObject();
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set additional Address
     *
     * @param string $additionalAddress
     *
     * @return self
     */
    public function setAdditionalAddress($additionalAddress)
    {
        $this->additionalAddress = $additionalAddress;

        return $this->updateAddressObject();
    }

    /**
     * Get additional Address
     *
     * @return string
     */
    public function getAdditionalAddress()
    {
        return $this->additionalAddress;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this->updateAddressObject();
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Resume
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this->updateAddressObject();
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Resume
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this->updateAddressObject();
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return self
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this->updateAddressObject();
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return self
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this->updateAddressObject();
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Get latLong
     *
     * @return array
     */
    public function getLatLong()
    {
        return [
            'lat'  => $this->getLatitude(),
            'long' => $this->getLongitude(),
        ];
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return self
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this->updateAddressObject();
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Update address Object
     *
     * @return self
     */
    protected function updateAddressObject()
    {
        if (empty($this->addressObject)) {
            $this->addressObject = new Address();
            $this->addressObject
                ->setAddress($this->address)
                ->setAdditionalAddress($this->additionalAddress)
                ->setCity($this->city)
                ->setZipcode($this->zipcode)
                ->setRegion($this->region)
                ->setCountry($this->country)
                ->setLatitude($this->latitude)
                ->setLongitude($this->longitude)
            ;
        }

        return $this;
    }

    /**
     * Has address
     *
     * @return boolean
     */
    protected function hasAddress()
    {
        if ($this->getAddress()
            || $this->getCity()
            || $this->getZipcode()
            || $this->getRegion()
            || $this->getAdditionalAddress()
            || $this->getCountry()) {
            return true;
        }

        return false;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getFullAddress()
    {
        $address = $this->getAddress();

        if (!empty($this->getZipcode())) {
            $address = sprintf('%s %s', $address, $this->getZipcode());
        }

        if (!empty($this->getCity())) {
            $address = sprintf('%s %s', $address, $this->getCity());
        }

        if (!empty($this->getAdditionalAddress())) {
            $address = sprintf('%s %s', $address, $this->getAdditionalAddress());
        }

        if (empty($this->getRegion())) {
            $address = sprintf('%s %s', $address, $this->getRegion());
        }

        if (!empty($this->getCountry())) {
            $address = sprintf('%s %s', $address, $this->getCountry());
        }

        return trim($address);
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddressObject()
    {
        $this->updateAddressObject();

        return $this->addressObject;
    }

    /**
     * Set address
     * @param Address $addressObject
     *
     * @return self
     */
    public function setAddressObject(Address $addressObject)
    {
        $this->addressObject     = $addressObject;
        $this->address           = $this->addressObject->getAddress();
        $this->additionalAddress = $this->addressObject->getAdditionalAddress();
        $this->city              = $this->addressObject->getCity();
        $this->zipcode           = $this->addressObject->getZipcode();
        $this->country           = $this->addressObject->getCountry();
        $this->region            = $this->addressObject->getRegion();
        $this->latitude          = $this->addressObject->getLatitude();
        $this->longitude         = $this->addressObject->getLongitude();

        return $this;
    }
}
```
