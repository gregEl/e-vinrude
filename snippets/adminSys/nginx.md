# Nginx

## Installation

`sudo apt install nginx`

Pour un installation avec php, il faut que php-fpm soit installé (pour la version 7.2 de php, le paquet se nomme `php7.2-fpm`)

## Configuration de base

La configuration par défaut suffit amplement pour travailler, mais les fichiers existants se trouvent dans le dossier `/etc/nginx/`. Pour ajouter des configuration, il est conseillé de le faire dans `/etc/nginx/conf.d/`

## Créer un "server" (vhost)

Pour utiliser un serveur local (disons un site `foo.local`), créer le fichier `/etc/nginx/sites-available/foo.local.conf` (le nom du fichier peut être comme vous le souhaitez, le `.conf` final est le seul élément standard.

Un exemple de fichier basique pour un site utilisant php-fpm (version 7.2) :

```nginx
server {
    server_name foo.local;
    root /var/www/foo.local;

    location / {
        index index.php;
        location ~ ^/(.+\.php)$ {
            try_files $uri =404;
            fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include /etc/nginx/fastcgi_params;
        }
    }

    error_log /var/log/nginx/foo/error.log;
    access_log /var/log/nginx/foo/access.log;

    listen 80;
}
```

Pour que tout fonctionne, il faut que les dossiers `/var/log/nginx/foo/` et `/var/www/foo.local` existent et soient utilisables par l'utilisateur `www-data`. Pour se faire, utiliser les commandes suivantes :

- `sudo mkdir -p /var/log/nginx/foo/`
- `sudo chown www-data:votre_nom_d_utilisateur -R /var/www/foo.local`

Une autre étape nécessaire est la modification du fichier `/etc/hosts` et ajouter une ligne pour chaque domaine à utiliser :

```
127.0.0.1   foo.local
127.0.0.1   www.foo.local
127.0.0.1   admin.foo.local
127.0.0.1   api.foo.local
```

### Activer le serveur

Pour que le serveur fonctionne, il nous faut notre config dans `/etc/nginx/sites-enabled/`. Pour l'avoir rangée dans `/etc/nginx/sites-available/` ? Pour garder une version que l'on peut désactiver **sans la supprimer**.

Pour l'activer : `sudo ln -s /etc/nginx/sites-available/foo.local.conf /etc/nginx/sites-enabled`
(ce qui créera un lien symbolique dans `/etc/nginx/sites-enabled` et qui pointera vers le fichier `/etc/nginx/sites-available/foo.local.conf`)

## Gérer l'état du service

### Vérifier l'état

Pour voir si nginx est démarré ou non (et éventuellement l'erreur associée) :
`sudo systemctl status nginx`

### Démarrer / arrêter le service

Démarrer nginx : `sudo systemctl start nginx`
Arrêter et relancer nginx : `sudo systemctl restart nginx`
Arrêter nginx : `sudo systemctl stop nginx`
