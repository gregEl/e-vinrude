# Proftpd

## Documentation 

* [http://www.proftpd.org/](http://www.proftpd.org/)
* [https://doc.ubuntu-fr.org/proftpd](https://doc.ubuntu-fr.org/proftpd)

## Configuration

*cat /etc/proftpd/conf.d/proftpd.conf*
```
ServerName Saonoise FTP
DefaultAddress 79.137.42.146
SocketBindTight on
DefaultRoot ~
AuthOrder mod_auth_file.c
AuthUserFile /etc/ftpd.passwd
RequireValidShell Off
PassivePorts 10000 10030
```


*cat /etc/proftpd/conf.d/tls.conf*
```
<IfModule mod_tls.c>
TLSEngine On
TLSLog /var/log/proftpd/tls.log
TLSProtocol TLSv1
TLSCipherSuite AES256+EECDH:AES256+EDH
TLSOptions NoCertRequest AllowClientRenegotiations
TLSRSACertificateFile /etc/proftpd/ssl/ftp.saonoise.com.combined.pem
TLSRSACertificateKeyFile /etc/proftpd/ssl/ftp.saonoise.com.key.pem
TLSRequired On
RequireValidShell Off
```

