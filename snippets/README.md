# Les snippets

## Administration Système

### [Ligne de commande](/snippets/adminSys/commandLine.html)

Explication de commandes de base (Linux/Ubuntu)

### [Nginx](/snippets/adminSys/nginx.html)

Configurer et utiliser Nginx

### [Utilisation de apt sur Debian/Ubuntu](/snippets/adminSys/ubuntuApt.html)

Utiliser apt pour installer des logiciels sous Debian/Ubuntu

## Front

### [Feuille de reset CSS](/snippets/cssReset.html)

Une feuille de reset pour plus simplement gérer les styles sur un projet

## Quotidien du développeur

### [Astuces Git](/snippets/git.html)

Des astuces pour utiliser Git au quotidien le plus confortablement possible

### [Makefile](/snippets/makefile.html)

Un fichier Makefile pour un projet Symfony + Docker (avec Nginx et MySql)
