# Installation Ubuntu

## Paquets Ubuntu

### Sans php local (utilisez Docker)

```bash
sudo apt install ansible curl flashplugin-installer gimp git gitk htop python rar tar thefuck unrar unzip vim vlc zip zsh
```

Sautez ensuite à [la section sur Docker](#docker)

### Avec un php local

```bash
sudo add-apt-repository ppa:ondrej/php
sudo add-apt-repository ppa:ondrej/nginx
```

```bash
sudo apt install ansible bind9-host curl flashplugin-installer gimp git gitk htop imagemagick imagemagick-common meld mysql-client mysql-server nginx pandoc php-pear php7.4 php7.4-cli php7.4-common php7.4-curl php7.4-dev php7.4-fpm php7.4-gd php7.4-intl php7.4-json php7.4-mbstring php7.4-mysql php7.4-readline python rar ruby ruby-dev tar thefuck unrar unzip vim vlc zip zsh
```

```bash
sudo pecl install xdebug
```
Ajouter ensuite `zend_extension=/usr/lib/php/XXX/xdebug.so` à la fin de votre fichier php.ini (`/etc/php/7.4/nginx/php.ini`)
(comme doit vous le préciser l'installation ;) )

## Docker

Supprimez les anciens paquets éventuellement installés 

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
```

Ajoutez les dépendances et le repository

```bash
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

Installez Docker et docker-compose 

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose
```

Assurez-vous que votre utilisateur appartienne au groupe `docker` et lui dire de démarrer au lancement de l'ordinateur

```bash
sudo groupadd docker
sudo usermod -aG docker YOURUSER
sudo systemctl enable docker
```

Pour que l'installation soit complètement opérationnelle, il faut *fermer et rouvrir votre session* ou *redémarrer l'ordinateur*.

Pour modifier le répertoire de stockage des containers, suivre les commandes suivantes : 

```bash
sudo service docker stop
mv /var/lib/docker /[MON_REPERTOIRE]/
sudo ln -s /[MON_REPERTOIRE]/docker /var/lib/docker
sudo service docker start
```

## NodeJs & NPM (optionnel si vous utilisez Docker)

```bash
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt install -y nodejs
sudo npm install -g gulp jshint
```

## Oh-my-zsh

Premièrement, changer de shell (passer de bash, installé par défaut, à zsh) :

```bash
chsh
```

Entrez votre mot de passe puis, lorsqu'on vous demande le nouveau shell à utiliser : 

```bash
/bin/zsh
```

Ensuite, installez Oh-My-Zsh:

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

Vous pouvez ensuite lancer un nouveau terminal pour en voir les effets ou relancer votre session ;) .

### Config and problems

Pour une [documentation plus complète, voir la documentation dédiée](/cookbooks/oh-my-zsh.html) sur ce site. 

## PhpStorm

Installez PhpStorm

```bash
sudo snap install phpstorm --classic
```

Puis configurez un raccourci (les applications Snap semblent mal fonctionner de ce côté avec KUbuntu et zsh) :

`vim /usr/share/applications/jetbrains-phpstorm.desktop`

```bash
[Desktop Entry]
Version=1.0
Type=Application
Name=PhpStorm
Icon=/snap/phpstorm/current/bin/phpstorm.png
Exec="/snap/phpstorm/current/bin/phpstorm.sh" %f
Comment=Un IDE pour PHP
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-phpstorm
```

## Composer

```bash
sudo apt update
sudo apt install php-cli unzip
curl -sS https://getcomposer.org/installer -o composer-setup.php
HASH=`curl -sS https://composer.github.io/installer.sig`
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm -rf composer-setup.php
```

Vérifiez

```bash
composer --version
```

## Spotify

Dans la même veine que PHPStorm, nous allons utiliser Snap pour install Spotify (pour ceux qui sont intéressés) : 

```bash
snap install spotify
```

Une fois cela fait, ajoutez un fichier `/usr/share/applications/spotify.desktop` : 

```ini
[Desktop Entry]
Type=Application
Name=Spotify
GenericName=Music Player
Icon=/snap/spotify/current/usr/share/spotify/icons/spotify-linux-128.png
TryExec=/snap/bin/spotify
Exec=/snap/bin/spotify %U
Terminal=false
MimeType=x-scheme-handler/spotify;
Categories=Audio;Music;Player;AudioVideo;
StartupWMClass=spotify
```
