# La première mise en prod/démo d'un projet Symfony

## Contenu

- [Pré-requis](#pre-requis)
- [Mise en place du Nginx sur le serveur](#nginx)
- [Mise en place du déploiement (Ansible)](#ansible)
- [Ajout du certificat ssl (si besoin)](#certificat)
- [Utilisation de Gitlab-ci pour une intégration continue](gitlab) 

## Pré-requis

Je pars du principe que le projet :

- le site sera déployé sur test.example
- est en symfony 4 ou 5,
- dispose d'un makefile (et une commande `make update` pour installer et construire le projet),
- passe par Gitlab et notre serveur Nginx actuel,
- va être déployé avec Ansible
- utilisera Gitlab-ci pour automatiser tests et déploiements

## Nginx

Un fichier nginx pour le site test.example (je pars du principe qu'il s'agit d'un site de démo). Les commentaires du fichiers disent tout ;) .

fichier `/etc/nginx/sites-available/test.example.conf`

```nginx
server {
    # Les noms de domaines correspondants au site.
    # test.example *.test.example sont suffisant, mais ajouter le domaine "www.", 
    # ou les autres sous-domaines important, permet de gagner un peu en performance.
    # En somme, on précise uniquement les sous-domaines essentiels 
    # (dans notre cas, "www." est celui sur lequel on tape systématiquement, le gain sera intéressant).
    server_name 
        www.test.example test.example *.test.example
    ;

    # Le dossier public de Symfony en chemin absolu
    root /var/www/test.example/current/public;
    
    # Ici, on veut rediriger le visiteur qui arriverait sur test.example vers www.test.example
    if ($host = test.example) {
        return 301 https://www.test.example$request_uri;
    }
    
    # Ici, on essaie de servir le fichier demandé, en priorité. S'il n'existe pas, on envoie la requête à index.php.
    # C'est comme ça que l'on peut servir directement les images.
    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    # On demande un mot de passe à l'utilisateur. 
    #Les mots de passe et identifiants disponibles sont dans le fichier /var/www/test.example/.htpasswd 
    # (les mots de passe sont hashés dans ce fichier)
    auth_basic           "Nope";
    auth_basic_user_file /var/www/test.example/.htpasswd;

    # La configuration pour php-fpm. Ne pas oublier de remplacer php7.2-fpm.sock par la version utilisée par le serveur.
    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        # Des variables d'environnement spécifiques au projet
        fastcgi_param APP_ENV prod;
        fastcgi_param APP_SECRET azertyuiopqsdfghjklm0123456789;
        fastcgi_param DATABASE_URL mysql://example:azerty123@127.0.0.1:3306/example;
        fastcgi_param MAILER_URL "smtp://smtp.mailtrap.io:2525?encryption=tls&auth_mode=login&username=CeciEstFaux&password=LuiAussi";
        fastcgi_param APP_DOMAIN test.example;

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }
    
    # On range les logs dans un dossier spécifique au projet 
    # et on nomme les logs par rapport à l'environnement (demo, staging ou prod, par exemple)
    error_log /var/log/nginx/example/demo.error.log;
    access_log /var/log/nginx/example/demo.access.log;
}
```

Il faut bien penser à créer le dossier de logs, s'il n'existe pas déjà : 

```bash
sudo mkdir -p /var/log/nginx/example
```

Il nous faut ensuite avoir les fichiers déployés pour pouvoir tester (et donc activer cette configuration Nginx).

## Ansible

Pour le déploiement, nous allons utiliser Ansistrano, et plus précisément le rôle [cbrunnkvist.ansistrano-symfony-deploy](https://github.com/cbrunnkvist/ansistrano-symfony-deploy).

Tout d'abord, il va nous falloir ajouter les tâches dans le Makefile :

```Makefile
#
# Déploie sur le serveur
#
deploy:
	ansible-playbook -i config/deploy/demo config/deploy/deploy.yml
.PHONY: deploy

#
# Installe les rôles ansible du projet
#
ansible.roles:
	ansible-galaxy install cbrunnkvist.ansistrano-symfony-deploy -p config/deploy/roles --ignore-errors
```

Idéalement, la tâche `ansible.roles` est appelée lors du setup du projet. Ce qui donne quelque chose comme ceci :

```Makefile
setup: ansible.roles install build
```

Lorsque nous utiliserons Gitlab-ci, cette tâche n'aura plus vraiment besoin d'être dans le setup (nous y reviendront plus tard ;) ).

Pour télécharger nos rôles, il faut d'abord créer les dossiers (si nécessaire) et lancer notre commande :

```bash
mkdir -p config/deploy/roles && make ansible.roles
```

Maintenant, créons nos fichiers et dossiers. Notre organisation va ressembler à ceci : 

![Image d'illustration de l'organisation des fichiers](./firstTimeDeploy.png)

Commençons par le bas ! Nous allons créer un fichier demo (plutôt que prod), contenant :

```ini
[demo-webservers]
; Ici on va remplacer l'adresse IP, le port et le nom du user à utiliser
; par des données un peu plus réelles ;)
127.0.0.1 ansible_port=22 ansible_connection=ssh ansible_user=deployer

[demo:children]
demo-webservers
```

Passons au fichier `deploy.yml` :

```yaml
---
-
  name: Deploy Application # Nom de la tâche, qui va s'afficher dans la commande, lors du déploiement
  hosts: all # les hôtes concernés par cette tâche (pour nous, demo/prod)
  gather_facts: false
  # Différentes variables que l'on va passer à notre rôle de déploiement
  vars:
    # playbook_dir est une variable définie et gérée par ansible et contenant le chemin vers le dossier où se trouve notre fichier utiliser, ici le dossier de deploy.yml (config/deploy).
    ansistrano_deploy_from: "{{ playbook_dir }}/../../" 
    ansistrano_deploy_to: "{{ deploy_to }}" # variable définie dans group_vars/demo.yml
    ansistrano_keep_releases: 5 # on choisi le nombre de release à conserver
    ansistrano_deploy_via: "git"
    ansistrano_git_repo: git@github.com:example/test.git
    ansistrano_shared_paths: # les chemins partagés d'une release à une autre
      - var/log
      - var/uploads
      #- var/files
      #- var/sessions
      #- public/cache
      #- public/media
    symfony_console_path: 'bin/console'
    # On désactive toutes les tâches automatiques du rôle, on y préfère notre Makefile
    symfony_run_assetic_dump: false
    symfony_run_assets_install: false
    symfony_run_doctrine_migrations: false
    symfony_run_composer: false
    symfony_run_cache_clear_and_warmup: false
    ansistrano_allow_anonymous_stats: false # Je n'aime pas trop contribuer à ansistrano ;) 
    # Ici, on déclare les hooks que l'on va utiliser. 
    # Pour une liste complète, voir là : https://github.com/cbrunnkvist/ansistrano-symfony-deploy#hooks
    # et là : https://github.com/ansistrano/deploy#role-variables
    # Nous allons avoir 3 hooks :
    # - avant setup : au tout début du déploiement, nous avons une tâche pour notifier notre Discord du déploiement
    # - avant que le lien symbolique "current" soit mis en place : on va utiliser notre Makefile pour mettre notre projet en place (appliquer les migrations, build le front, etc.)
    # - à la toute fin du déploiement : on notifie notre discord que tout s'est bien passé et on s'assure que les droits de toutes les releases ont les bons droits
    ansistrano_before_setup_tasks_file: "{{ playbook_dir }}/config/steps/before-setup.yml"
    ansistrano_before_symlink_tasks_file: "{{ playbook_dir }}/config/steps/before-symlink.yml"
    ansistrano_after_cleanup_tasks_file: "{{ playbook_dir }}/config/steps/after-cleanup.yml"
  roles: # On appelle les rôles nécessaires pour que notre tâche fonctionne sans accro
    - { role: cbrunnkvist.ansistrano-symfony-deploy }
  environment: # On peut définir des variables d'environnement à utiliser pendant le déploiement
    BASE_URL: "{{ app_base_url }}"
    HOST: "{{ app_host }}"
    SCHEME: "{{ app_scheme }}"
```

Continuons de remonter les fichiers, et passons au dossier `group_vars`, où nous allons créer un fichier `demo.yml` :

```yaml
---
deploy_to: /var/www/example.test
symfony_php_path: php
front_env: prod
app_env: prod
app_base_url: /
app_host: www.example.test
app_scheme: https
app_domain: www.example.test
app_name: ExampleTest
app_server: demo
``` 

Ces variables seront utilisables dans nos tâches (et sont utilisées par exemple dans notre fichier deploy.yml). Tout comme le fichier prod à la racine, ce fichier nous permet d'avoir une configuration spécifique pour chaque environnement.

Passons au dossier step :

- Le fichier `before-symlink.yml` :

```yaml
---
- name: Update Project
  shell: chdir={{ansistrano_release_path.stdout}} # on se place dans le dossier de la release
    make update ENV=prod # on appelle notre commande
```

Ici, on appelle seulement notre commande make update. Selon la configuration, le paramètre env peut être défini en variable d'environnement et peut être supprimé ici (c'est une meilleure pratique que de le passer en paramètre des commandes make).

- le fichier `before-setup.yml`: 

```yaml
---
- name: Notify Discord
  uri:
    url: https://discordapp.com/api/webhooks/cheminDuWebHookDonnéParDiscord
    body: "{ \"content\": \"**Début** du déploiement de {{ app_name }} sur le serveur **{{ app_server }}** !\" }"
    body_format: json
    method: POST
    status_code: [200, 204] # On considère le hook comme un succès si l'un de ces statuts est renvoyé.
```

Pour récupérer le chemin du webhook, il faut aller dans la configuration du canal discord où l'on veut envoyer la notification, et créer / modifier un webhook ;).

- enfin, le fichier `after-cleanup.yml`: 

```yaml
---
- name: Ensure www-data group on every releases
  file:
    path: '{{ deploy_to }}'
    state: directory
    group: www-data # Si besoin remplacer le group et le owner
    owner: www-data
    recurse: true

- name: Notify Discord
  uri:
    url: https://discordapp.com/api/webhooks/cheminDuWebHookDonnéParDiscord
    body: "{ \"content\": \"**Réussite** du déploiement de {{ app_name }} sur le serveur **{{ app_server }}** ! :D Il est temps de tester sur https://{{ app_host }} !\" }"
    body_format: json
    method: POST
    status_code: [200, 204]
```

Normalement, il ne reste plus qu'à tester en lançant la commande `make deploy` et à éventuellement adapter la commande `update` ou les variables d'environnement transmises. 

Une fois le déploiement réussi, il ne nous reste plus qu'à activer notre configuration nginx :

```bash
sudo ln -s /etc/nginx/sites-available/test.example.conf /etc/nginx/sites-enabled
sudo service nginx restart
```

Si notre fichier est bien fait, il ne devrait pas y avoir de problème. Si une erreur se produit, on va supprimer le lien symbolique `sudo rm /etc/nginx/sites-enabled/test.example.conf`, relancer nginx et déboguer notre configuration avec un outil comme cette commande : 

```bash
sudo nginx -c /etc/nginx/sites-available/test.example.conf -t
```

## Certificat

Dans un premier temps, il faut se connecter sur le serveur *via* SSH, avec un utilisateur ayant les droits sudo.

Il faut que notre site soit déjà accessible en ligne (il est aussi possible de valider le domaine directement, via CloudFlare, mais je ne recommande pas cette option ici).
Ensuite, on va demander à Certbot de nous générer un certificat ET de l'ajouter à notre config Nginx :

```bash
sudo certbot --nginx -d "test.example" -d "*.test.example"
```

Si besoin, on peut ajouter d'autres domaines avec `-d`, ce qui permet d'avoir un certificat pour tous les domaines liés à un site, comme les `.com`, `.fr` et `.org`.

Il suffit ensuite de suivre les instructions de certbot !

## Gitlab

### Bases théoriques et vocabulaire

Gitlab permet d'automatiser des tâches (build un projet, lancer des tests, déployer sur différents environnements, etc.) de manière automatisée, dans ce que l'on appelle des Pipelines.
Ces pipelines sont composés d'un ou plusieurs stages (étapes), qui peuvent être composés de plusieurs jobs (tâches).

Concrètement, Gitlab exécute les différents jobs d'un pipeline grâce à des runners, de petits programmes fait en Go, qui s'activent sur chaque projet. Pour notre installation, nous allons utiliser un runner global existant.

Un runner permet de définir un contexte dans lequel les jobs seront effectués. Les différentes options incluent : 
- le shell de la machine où est installé le Gitlab,
- le shell d'une machine avec un accès ssh,
- une machine docker créée pour l'occasion,
- une machine docker avec un accès ssh
- etc. (liste des options possibles : custom, ssh, docker+machine, docker-ssh+machine, kubernetes, docker, docker-ssh, parallels, shell, virtualbox)

Pour l'heure, nous utilisons des runners shell (nous avons peu de dépendances et la machine du Gitlab les respectent bien) et passerons sûrement sur docker/docker+machine à l'avenir. 

### Préparation

Tout d'abord, il nous faut un runner, pour exécuter notre CI. Pour cela, il faut aller dans les settings, section "CI/CD", partie runners. Pour l'heure, il suffit d'activer l'un des Shared runners (Global).

Vous trouverez également une section "environment variables", qui vous permet de gérer les variables d'environnement à transmettre aux différents jobs ! Par la même occasion, ça nous permet d'enlever les informations sensibles de nos fichiers Ansible ;) .

Plus bas dans cette page, vous pouvez voir les "deploy keys". Il est conseillé d'utiliser une clé disponible dans "Publicly accessible deploy keys", qui sont configurées pour fonctionner sur nos environnements ;).

Une fois que nous avons tout ça, nous allons pouvoir passer à la création de jobs, stages et pipelines !

### Le fichier .gitlab-ci.yml

Voici un exemple de fichier (utilisé pour nos sites Symfony), avec des commentaires détaillant les différentes étapes.

```yaml
# Pour réaliser ce fichier, je me suis basé sur le tuto d'Eleven Labs : https://blog.eleven-labs.com/fr/introduction-gitlab-ci/

# On définit les différentes étapes (stages) à faire tourner dans le pipeline
stages:
    - build # cette étape (stage) va compiler l'application, permettant de vérifier qu'il n'y ait pas d'erreur dès la compilation
    - analysis # Analyse le code de l'application (analyses statique / lint)
    - deploy # Déploie l'application sur le serveur approprié, si nécessaire

image: drakona/php:7.2-ci # On va lancer ces jobs dans un container Docker. Ici, notre image php, un version spécifique au CI en php7.2

services: # On va utiliser d'autres images Docker en dépendances, ici une base de données MySQL (version 5.7)
    - mysql:5.7

variables: # Des variables d'environnement que l'on va passer aux containers qu'on appelle (ici la configuration de la base de données)
    MYSQL_ROOT_PASSWORD: pass
    MYSQL_USER: site
    MYSQL_PASSWORD: pass
    MYSQL_DATABASE: site

# On définit une tâche (job) qui pourra être lancée lors d'un stage
build:
    stage: build # Ce job sera exécuté lors du stage build
    script: # Le script que va utiliser ce job. Ici, on profite de notre Makefile !
        - make packages.init db.update build
    cache: # On met certains dossiers en cache, pour les conserver d'une exécution à l'autre
        paths:
            - vendor/ # Ici, on garde le dossier vendor, afin de ne pas re-télécharger toutes les dépendances à chaque fois
    artifacts:
        paths:
            - public
    tags:
        - docker
        - php7.2

analysis:
    stage: analysis
    script:
        - make packages.init analysis
    cache: # On met certains dossiers en cache, pour les conserver d'une exécution à l'autre
        paths:
            - vendor/ # Ici, on garde le dossier vendor, afin de ne pas re-télécharger toutes les dépendances à chaque fois
    allow_failure: true
    tags:
        - docker
        - php7.2

lint:
    stage: analysis
    script:
        - make packages.init lint
    cache: # On met certains dossiers en cache, pour les conserver d'une exécution à l'autre
        paths:
            - vendor/ # Ici, on garde le dossier vendor, afin de ne pas re-télécharger toutes les dépendances à chaque fois
    allow_failure: true
    tags:
        - docker
        - php7.2

deploy.demo:
    stage: deploy
    script: make ansible.roles deploy.demo
    environment:
        name: demo
        url: https://www.site.demo
    only:
        - demo

# Un autre job, qui va déployer notre application sur le serveur de prod
deploy.prod:
    stage: deploy # Ce job se lance lors du stage deploy
    script: make ansible.roles deploy.prod # Le script à exécuter
    environment:
        name: prod
        url: https://www.site.prod
    only: 
        - master # Ce déploiement n'a lieu que si l'on fait une action sur master (commit, push, merge, etc.)
    when: manual    
```

Si toute la configuration est en ordre, nos pipelines commencent directement à tourner. Avec cette configuration, le stage build sera exécuté à chaque modification, quelle que soit la branche, et le deploy quand des modifications sont faites sur master.

![Image d'illustration d'un pipeline](./firstTimeDeploy_pipeline.png)
![Image d'illustration de jobs](./firstTimeDeploy_jobs.png)

En cas d'erreur, vous avez accès à tous les logs dans les pages `CI/CD > Pipelines` du repository.

Il peut y avoir des réglages supplémentaires à effectuer (dans l'exemple de nos projets VuePress déployés avec Ansistrano, nous avons eu besoin d'ajouter les clés de déploiement directement dans le projet).
