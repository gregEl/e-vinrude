# Couleurs / Textes / Mise en forme

Cette page détaille le système de classe dédié à l'application des propriétés dites "d'habillage", tel que l'application de couleurs, la mise en forme de texte, l'ajout de bordures, etc.

## Syntaxe

Les classes appartenant à ce système sont identifiables par un Préfixe `ev-`, suivi d'un Block `{nom de propriété}`, d'un Complément `{__nature}` puis d'un Suffixe (ou "modifier") de type `--{valeur}`.

**Exemples de syntaxes :**

- Afficher un texte en "bold" : `<div class="ev-text__weight--bold">Mon texte</div>`

## Texte

Les classes appliquant des propriétés de texte se composent du Préfixe `ev-`, suivi d'un Block `{propriété}`, d'un Complément `__color`, puis d'un Suffixe de type `--{couleur}`.
> Les couleurs disponibles sont spécifiées dans la variable `$colors` de "_colors.scss".

<br/>

### "ev-text__size"

Permet de modifier la taille d'un texte.
> - Nécessite l'emploi d'un suffixe de type `--{text-size}`.
> - Les tailles disponibles sont spécifiées dans la variable `$text-sizes`  de "_fonts.scss".

**Exemple :**

- **ev-text__size--default** : Applique le style `font-size: 1rem;`

<br/>

### "ev-text__weight"

Définir l'épaisseur d'un texte.
> - Nécessite l'emploi d'un suffixe de type `--{text-weight}`.
> - Les épaisseurs disponibles sont spécifiées dans la variable `$text-weights` de "_fonts.scss".

**Exemple :**
- `"ev-text__weight--bold"` : Applique le style `font-weight: 600;`

<br/>

### "ev-text__style"

Définir le style à donner à du texte.
> Nécessite l'emploi d'un suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--normal` : Applique le style `font-style: normal;`
- `--italic` : Applique le style `font-style: italic;`

<br/>

### "ev-text__transform"

Définir le type de transformation à appliquer à du texte.
> Classe à utiliser avec suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--capitalize` : Applique le style `text-transform: capitalize;`
- `--uppercase` : Applique le style `text-transform: uppercase;`
- `--lowercase` : Applique le style `text-transform: lowercase;`
- `--none` : Applique le style `text-transform: none;`

<br/>

### "ev-text__decoration"

Permet d'ajouter ou de retirer des soulignages et surlignages à du texte.
> Classe à utiliser avec suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--underline` : Applique le style `text-decoration: underline;`
- `--overline` : Applique le style `text-decoration: overline;`
- `--none` : Applique le style `text-decoration: none;`

<br/>

### "ev-text__wrap"

Permet de contraindre une texte à ne s'afficher que sur une seule ligne.
> Classe à utiliser avec suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--nowrap` : Applique le style `white-space: nowrap;`
- `--wrap` : Applique le style `white-space: normal;`

<br/>

### "ev-text__ellipsis"

Contraindre un texte à ne s'afficher que sur une seule ligne en le "coupant" à l'aide de points de suspension.
> Nécessite de définir le "max-width" du container parent (max-width 100% appliqué par défaut).

<br/>

### "ev-text__align"

Définir la justification/alignement d'un texte.
> Classe à utiliser avec suffixe de type `--{suffixe}`, parmis ceux proposés dans la liste ci-dessous.

**Suffixes disponibles :**
- `--left` : Applique le style `text-align: left;`
- `--right` : Applique le style `text-align: right;`
- `--center` : Applique le style `text-align: center;`
- `--justify` : Applique le style `text-align: justify;`

<br/>

### Responsive : "ev-text__small-align" & "ev-text__medium-align"

Permet de gérer la justification ("text-align") du texte en fonction des différentes tailles d'affichage.
> Les tailles d'affichages "small" et "medium" sont définies par les variables `$breakpoint-medium` et `$breakpoint-small` de "_variable.scss".

**Suffixes disponibles :**
- `--left` : Applique le style `text-align: left;`
- `--right` : Applique le style `text-align: right;`
- `--center` : Applique le style `text-align: center;`

<br/>

### "ev-text-shadow"

Classe permettant de supprimer les ombres appliquées à du texte.

- **"ev-text-shadow--none"** : Applique le style `text-shadow: none;`
> Supprime une ombre appliquée à un texte.

## Couleurs

Les classes appliquant des propriétés de couleur se composent du Préfixe `ev-`, suivi d'un Block `{propriété}`, d'un Complément `__color` (ou `__color-hover` pour la gestion du survol), puis d'un Suffixe de type `--{couleur}`.
> Les couleurs disponibles sont spécifiées dans la variable `$colors` de "_colors.scss".

<br/>

### "ev-color__text" & "ev-color__text-hover"

Permet d'appliquer une couleur d'un texte'.
> - Nécessite l'emploi d'un suffixe de type `--{couleur}`.
> - Les couleurs disponibles sont spécifiées dans la variable `$colors` de "_colors.scss".

**Exemple :**

- **"ev-color__text--gray-20"** : Applique le style `background-color: #202020;`

<br/>

### "ev-color__background" & "ev-color__background-hover"

Permet d'appliquer une couleur de fond à une balise HTML.
> - Nécessite l'emploi d'un suffixe de type `--{couleur}`.
> - Les couleurs disponibles sont spécifiées dans la variable `$colors` de "_colors.scss".

**Exemple :**

- **"ev-color__background--gray-20"** : Applique le style `background-color: #202020;`

<br/>

### "ev-color__border" & "ev-color__border-hover"

Permet de modifier la couleur d'une bordure.
> - Nécessite l'emploi d'un suffixe de type `--{couleur}`.
> - Les couleurs disponibles sont spécifiées dans la variable `$colors` de "_colors.scss".
> - Pour supprimer une bordure, utiliser la classe `"ev-border--none"`.

**Exemple :**

- **"ev-color__border--gray-20"** : Applique le style `border-color: #202020;`

<br/>

### "ev-color__before" & "ev-color__before-hover" (et variante "after")

Permet de modifier la couleur des pseudo-elements `::before` et `::after`.
> - Nécessite l'emploi d'un suffixe de type `--{couleur}`.
> - Les couleurs disponibles sont spécifiées dans la variable `$colors` de "_colors.scss".
> - Pour supprimer un pseudo élément, utiliser les classes `"ev-before--none"` ou `"ev-after--none"`.

**Exemple :**

- **"ev-color__border--gray-20"** : Applique le style `border-color: #202020;`

## Divers

### "ev-shadow"

Classe permettant de supprimer les ombres appliquées à une balise.

- **"ev-shadow--none"** : Applique le style `box-shadow: none;`
