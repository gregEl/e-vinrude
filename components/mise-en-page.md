
# Layout / Mise en page

Cette page détaille le fonctionnement du système de classes basés sur la technologie 'Flexbox', régissant le comportement de l'ensemble des éléments génériques de mise en page HTML.
<br/>
Ces classes ont pour vocation de permettre une mise en place rapide et standardisée de la majorité des éléments constituant les différentes pages d'un site, tout en limitant la quantité de CSS utilisée.

[**> Guide complet : Flexbox**](https://la-cascade.io/flexbox-guide-complet/)

## <span style="color: #3fc8ff;">■</span> Syntaxe

Les classes appartenant à ce système sont identifiables par un Préfixe `ev-`, suivi d'un Block `{block}` et d'un éventuel Complément `__{nature}`.
<br/>
Selon les cas de figures, cet ensemble peut être suivi d'un Suffixe (ou "modifier") de type `--{suffixe}`, lorsqu'il est possible d'ajouter un niveau de spécificité complémentaire.

**Exemples de syntaxes employées :**

- Base (Préfixe + Block) : `<div class="ev-section"></div>`

- Base + Complément : `<div class="ev-section__header"></div>`

- Base + Suffixe : `<div class="ev-justify--center"></div>`

- Base + Complément + Suffixe : `<div class="ev-element__wrapper--gutter"></div>`

## <span style="color: #3fc8ff;">■</span> Structure générale

### Layout standard

La structure la plus standard, se compose d'un container principal de classe `"ev-layout"`, lui-même découpé en 3 grands blocs : les balises `<header>`, `<main>` et `<footer>`.

**Exemple de layout classique :**

```html
<div class="ev-layout">
    <header class="header">Header</header>
    <main class="content">Content</main>
    <footer class="footer">Footer</footer>
</div>
```

<div class="ev-layout ev-text--center">
    <header class="header">Header</header>
    <main class="content ev-justify--center">Content</main>
    <footer class="footer">Footer</footer>
</div>

<br/>

### Layout à plusieurs colonnes

Dans le cas d'un layout constitué de plusieurs colonnes, pour ajouter un menu latéral par exemple, on ajoute au container `<main classe="ev-layout"`> la classe `"ev-layout--column"`.
<br/>
Ces classes ont pour simple fonction de donner au container principal la propriété `display: flex;` et de définir l'axe principal (main-axis) sur lequel doivent s'aligner les éléments qu'il contient (en rangées "row", ou en colonnes "column").

**Exemple de layout composé de plusieurs colonnes :**

```html
<div class="ev-layout ev-layout--column">
    <div class="column-1">
        <header class="header">Header</header>
        <main class="content">Content</main>
        <footer class="footer">Footer</footer>
    </div>
    <nav class="column-2 menu">
        Navigation
    </nav>
</div>
```

<div class="ev-layout ev-layout--column">
    <div class="column-1 ev-text--center">
        <header class="header">Header</header>
        <main class="content ev-justify--center">Content</main>
        <footer class="footer">Footer</footer>
    </div>
    <nav class="column-2 menu">
        Navigation
    </nav>
</div>

<br/>

### Structures complexes : Grid Layout

Pour la mise en place de structures globales plus complexes et/ou spécifiques, ainsi que pour la création de tableaux, on préfèrera l'utilisation de la technologie "Grid".
Très efficace pour la mise en place d'éléments purement structurels mais difficile standardiser, cette solution restera donc à déployer au cas par cas (voir guide).

[**> Guide complet : Grid Layout**](https://la-cascade.io/css-grid-layout-guide-complet/)

<br/>

### "ev-content" & "ev-section"

La classe `"ev-content"` permet de créer un container de type 'Container Flex' destiné à accueillir du contenu.
> Par défault, les éléments présents à l'intérieur d'une balise portant la classe `"ev-content"` s'affichent les uns en dessous des autres.
Pour un affichage en ligne, ajouter la classe `"ev-content--column"`.

<br/>

Les sections `"ev-section"` ont quant à elles vocation à permettre la subdivision d'un bloc de contenu en plusieurs sous-blocs indépendants, eux-mêmes composés d'un titre et d'un ou plusieurs blocs de contenus.

**Une "section" se compose de 2 types d'éléments :**

**1 -** Une balise `<header>` portant la classe `"ev-section__header"` contenant elle-même une balise "titre" (type `<h2>`) de classe `"ev-section__title"`
<br/>
**2 -** Une ou plusieurs balises dédiées au contenu de classe `"ev-section__content"`.

> - La valeur de la variable `$ev-section-padding` détermine l'espacement appliqué de chaque côté des `"section__header"` et `"section__content"` d'une section.
> - La valeur de la variable `$ev-section-margin` détermine la taille des marges appliquées autour d'une section.

**Exemple de "sections"  :**

```html
<main class="ev-content">
    <section class="ev-section">
        <div class="ev-section__header">
            <h2 class="ev-section__title">Titre section 1</h2>
        </div>
        <div class="ev-section__content">Contenu section 1</div>
        <div class="ev-section__content">Contenu section 1</div>
        <div class="ev-section__content">Contenu section 1</div>
    </section>
    <section class="ev-section">
        <div class="ev-section__header">
            <h2 class="ev-section__title">Titre section 2</h2>
        </div>
        <div class="ev-section__content">Contenu section 2</div>
        <div class="ev-section__content">Contenu section 2</div>
        <div class="ev-section__content">Contenu section 2</div>
    </section>
</main>
```

<main class="ev-content content">
    <section class="ev-section section">
        <div class="ev-section__header section__header">
            <h2 class="ev-section__title section__title">Titre section 1</h2>
        </div>
        <div class="ev-section__content section__content">Contenu section 1</div>
        <div class="ev-section__content section__content">Contenu section 1</div>
        <div class="ev-section__content section__content">Contenu section 1</div>
    </section>
    <section class="ev-section section">
        <div class="ev-section__header section__header">
            <h2 class="ev-section__title section__title">Titre section 2</h2>
        </div>
        <div class="ev-section__content section__content">Contenu section 2</div>
        <div class="ev-section__content section__content">Contenu section 2</div>
        <div class="ev-section__content section__content">Contenu section 2</div>
    </section>
</main>

**Exemple de sections affichées sur la même ligne à l'aide de la classe `"ev-content--column"` :**

```html
<main class="ev-content ev-content--column">
    <section class="ev-section">
        <div class="ev-section__header">
            <h2 class="ev-section__title">Titre section 1</h2>
        </div>
        <div class="ev-section__content">Contenu section 1</div>
        <div class="ev-section__content">Contenu section 1</div>
        <div class="ev-section__content">Contenu section 1</div>
    </section>
    <section class="ev-section">
        <div class="ev-section__header">
            <h2 class="ev-section__title">Titre section 2</h2>
        </div>
        <div class="ev-section__content">Contenu section 2</div>
        <div class="ev-section__content">Contenu section 2</div>
        <div class="ev-section__content">Contenu section 2</div>
    </section>
</main>
```

<main class="ev-content ev-content--column content">
    <section class="ev-section section">
        <div class="ev-section__header section__header">
            <h2 class="ev-section__title section__title">Titre section 1</h2>
        </div>
        <div class="ev-section__content section__content">Contenu section 1</div>
        <div class="ev-section__content section__content">Contenu section 1</div>
        <div class="ev-section__content section__content">Contenu section 1</div>
    </section>
    <section class="ev-section section">
        <div class="ev-section__header section__header">
            <h2 class="ev-section__title section__title">Titre section 2</h2>
        </div>
        <div class="ev-section__content section__content">Contenu section 2</div>
        <div class="ev-section__content section__content">Contenu section 2</div>
        <div class="ev-section__content section__content">Contenu section 2</div>
    </section>
</main>

<br/><br/>

## <span style="color: #3fc8ff;">■</span> "Layout 12 colonnes"

Ce système de layout a été pensé pour permettre d'agencer les éléments de contenus de manière simple et rapide, selon une "grille" composée de 12 colonnes.
La combinaison entre 'Container Flex' `"ev-element__wrapper"` et élément 'Flex' `"ev-element"` offre un large éventail de possibilités, et permet ainsi de répondre à la majorité des besoins en matière de mise en page.

### "ev-element__wrapper"

La classe `"ev-element__wrapper"` permet de définir un 'Container Flex' dans lequel afficher un ou plusieurs éléments 'Flex' (`"ev-element"`). Ce container est nécessaire si l'on souhaite pouvoir paramétrer le comportement et la répartition des éléments qu'il contient.

<br/>

### "ev-element"

Classe à appliquer à un élément se trouvant dans un container parent `"ev-element__wrapper"` afin de définir son comportement par rapport au dit container.

**Principe de fonctionnement :**

Les éléments `"ev-element"` cherchent naturellement à utiliser l'espace disponible en se répartissant de manière équitable. Ainsi, si un container ne contient qu'un seul élément, ce dernier utilisera 100% de la largeur. Pour deux éléments chacun occupe 50%, pour trois éléments chacun occupe 33,33%... et ainsi de suite jusqu'à un maximum de six.
<br/>
Des variantes de classes avec "modifier" ont également été ajoutées afin de permettre la combinaison d'éléments de différentes tailles dans un même container.

- **Utilisation pour des éléments de tailles équivalentes :**
<br/>
Il suffit de créer un container avec la classe `"ev-element__wrapper"` et d'y placer entre 1 et 6 éléments portant la classe `"ev-element`.

**Exemple :**

```html
<div class="ev-element__wrapper">
    <div class="ev-element">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3</div>
</div>
```
<div class="ev-content content">
    <div class="ev-element__wrapper">
        <div class="ev-element element">Élement 1</div>
        <div class="ev-element element">Élement 2</div>
        <div class="ev-element element">Élement 3</div>
    </div>
</div>


<br/>

- **Utilisation pour des éléments de tailles différentes :**
<br/>
Il est possible à l'aide d'une classe complémentaire `ev-element`, suivie d'un suffixe de type `--suffixe`, de spécifier l'espace qu'un élément doit occuper dans le container.
L'élément utilise alors l'espace souhaité, tandis que le ou les autres éléments dont la taille n'est pas spécifiée se répartissent l'espace restant de manière équitable.

**Exemple :**

```html
<div class="ev-element__wrapper">
    <div class="ev-element ev-element--half">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3</div>
</div>
```
<div class="ev-content content">
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--half element">Élement 1</div>
        <div class="ev-element element">Élement 2</div>
        <div class="ev-element element">Élement 3</div>
    </div>
</div>

<br/>

**Suffixes disponibles :**

- **"ev-element--half"** : l'élément occupe 1/2 de la largeur du container.

- **"ev-element--tier"** : l'élément occupe 1/3 de la largeur du container.

- **"ev-element--tier-2"** : l'élément occupe 2/3 de la largeur du container.

- **"ev-element--quarter"** : l'élément occupe 1/4 de la largeur du container.

- **"ev-element--quarter-3"** : l'élément occupe 1/4 de la largeur du container.

- **"ev-element--sixth"** : l'élément occupe 1/6 de la largeur du container.

- **"ev-element--sixth-5"** : l'élément occupe 5/6 de la largeur du container.

<br/>

**Tableau de démonstration :**

Ce tableau basé sur le système de layout 12 colonnes offre un aperçu des combinaisons qu'il est possible d'obtenir.
> Dans ces tableaux, les éléments dont la taille n'est pas définie porte le nom "Élément", tandis que ceux dont la taille est spécifiée portent le nom correspondant à leur suffixe (ex: "TIER-2" pour un élément de classe `"ev-element--tier-2"` et devant donc occuper 2/3 du container parent).

<section class="ev-content content">
    <div class="ev-element__wrapper">
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--half element">HALF</div>
        <div class="ev-element element">Élément</div>
        <div class="ev-element element">Élément</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element element">TIER</div>
        <div class="ev-element ev-element--tier-2 element">Élément</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--quarter element">QUARTER</div>
        <div class="ev-element element">Élément</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--sixth element">SIXTH</div>
        <div class="ev-element element">Élément</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--tier-2 element">TIER-2</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--quarter-3 element">QUARTER-3</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper">
        <div class="ev-element ev-element--sixth-5 element">SIXTH-5</div>
        <div class="ev-element element">Élement</div>
    </div>
</section>

<br/>

**Variante d'affichage avec "gouttières" (marges) :**

En ajoutant la classe complémentaire `"ev-element__wrapper--gutter"`, les éléments sont répartis de la même manière, mais séparés par des marges régulières.
> La valeur de la variable `ev-element-margin` détermine la taille des marges appliquées autour de chaque élément.

<section class="ev-content content">
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element ev-element--half element">HALF</div>
        <div class="ev-element element">Élément</div>
        <div class="ev-element element">Élément</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element element">TIER</div>
        <div class="ev-element ev-element--tier-2 element">Élément</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element ev-element--quarter element">QUARTER</div>
        <div class="ev-element element">Élément</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element ev-element--sixth element">SIXTH</div>
        <div class="ev-element element">Élément</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element ev-element--tier-2 element">TIER-2</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element ev-element--quarter-3 element">QUARTER-3</div>
        <div class="ev-element element">Élement</div>
    </div>
    <div class="ev-element__wrapper ev-element__wrapper--gutter">
        <div class="ev-element ev-element--sixth-5 element">SIXTH-5</div>
        <div class="ev-element element">Élement</div>
    </div>
</section>

<br/><br/>

## <span style="color: #3fc8ff;">■</span> Propriétés 'Flex' (hors layout 12 colonnes)

### "ev-basis"

Permet de paramétrer le 'flex-basis' d'un élément 'Flex', soit sa taille par défaut.
<br/>
> - Classe à utiliser avec un suffixe de type `--{valeur}`.
> - Les "valeurs" sont à remplacer soit par une valeur spécifiée dans la variable `$properties-size-values`, soit par une valeur numérique comprise entre les valeurs des variables `$properties-min-rem-value` et `$properties-min-rem-value` (unité "rem").

[**> Guide flexbox (flex-basis)**](https://la-cascade.io/flexbox-guide-complet/#flexbasis)

<br/>

### "ev-grow"

Définit la priorité avec laquelle un item 'Flex' doit s'agrandir lorsque l'espace occupé par l'ensemble des éléments est inférieur à l'espace total disponible.
<br/>
> - Par défaut, la propriété "grow" d'un élément flex a une valeur de 0
> - Classe à utiliser avec suffixe de type `--{valeur de 0 à 7}`

[**> Guide flexbox (flex-grow)**](https://la-cascade.io/flex-grow/)

**Exemple :**

```html
<div class="ev-content">
    <div>grow 0</div>
    <div class="ev-grow--1">grow 1</div>
    <div class="ev-grow--3">grow 3</div>
</div>
```
<div class="ev-content content">
    <div class="element">grow 0</div>
    <div class="ev-grow--1 element">grow 1</div>
    <div class="ev-grow--3 element">grow 3</div>
</div>

<br/>

### "ev-shrink"

Définit la priorité avec laquelle un item flex doit rétrécir lorsque l'espace occupé par l'ensemble des éléments est supérieur à l'espace total disponible.
<br/>
On cherchera généralement à définir une valeur de "shrink" pour des éléments situés à l'intérieur d'un 'Container Flex' dont on souhaite forcer l'affichage sur une seule ligne (avec propriété `flex-wrap: nowrap;`), dans un tableau par exemple.
<br/>
> - Par défaut, la propriété "shrink" d'un élément flex a une valeur de 1
> - Classe à utiliser avec suffixe de type `--type-{valeur de 1 à 7}`

[**> Guide flexbox (flex-shrink)**](https://la-cascade.io/flex-shrink/)

**Exemple :**

```html
<div class="ev-content ev-nowrap">
    <div class="ev-element">shrink 1</div>
    <div class="ev-element ev-shrink--3">shrink 3</div>
    <div class="ev-element ev-shrink--2">shrink 2</div>
</div>
```
<div class="ev-content ev-nowrap content">
    <div class="ev-element ev-basis--half element">shrink 1</div>
    <div class="ev-element ev-basis--half ev-shrink--3 element">shrink 3</div>
    <div class="ev-element ev-basis--half ev-shrink--2 element">shrink 2</div>
</div>

<br/>

## <span style="color: #3fc8ff;">■</span> Propriétés 'Flex' générales

### "ev-direction"

Permet de définir l'axe principal (main-axis) d'un 'Container Flex'.
> Classe à utiliser avec suffixe de type `--suffix`

[**> Guide flexbox (axis et bases)**](https://la-cascade.io/flexbox-guide-complet/#lesbases)
<br/>
[**> Guide flexbox (flex-direction)**](https://la-cascade.io/flexbox-guide-complet/#flexdirection)

**Suffixes disponibles :**
- **"ev-direction--row"** : Applique le style `flex-direction: row;`
> - Axe principal horizontal, avec "start" à gauche et "end" à droite
> - C'est l'axe défini par défaut pour un élément 'Flex'

**Exemple :**

```html
<div class="ev-content ev-direction--row">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-direction--row content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br>

- **"ev-direction--column"** : Applique le style `flex-direction: column;`
> - Axe principal vertical, avec "start" en haut et "end" en bas
> - Les éléments affichés dans un container ayant une répartition verticale occupent par défaut 100% de la largeur disponible

**Exemple :**

```html
<div class="ev-content ev-direction--column">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-direction--column content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

- **"ev-direction--row-reverse"** : Applique le style `flex-direction: row-reverse;`
> Axe principal horizontal, mais dont le "start" et le "end" sont inversés, soit de droite à gauche.

**Exemple :**

```html
<div class="ev-content ev-direction--row-reverse">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-direction--row-reverse content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br>

- **"ev-direction--column-reverse"** : Applique le style `flex-direction: column-reverse;`
> - Axe principal vertical, mais dont le "start" et le "end" sont inversés, soit de bas en haut
> - Les éléments affichés dans un container ayant une répartition verticale occupent par défaut 100% de la largeur disponible

**Exemple :**

```html
<div class="ev-content ev-direction--column-reverse">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-direction--column-reverse content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

### "ev-justify"

Permet de définir la manière dont sont répartis les éléments sur l'axe principal (main-axis) d'un 'Container Flex'.
> Classe à utiliser avec suffixe de type `--suffix` :

[**> Guide flexbox (axis et bases)**](https://la-cascade.io/flexbox-guide-complet/#lesbases)
<br/>
[**> Guide flexbox (justify-content)**](https://la-cascade.io/flexbox-guide-complet/#justifycontent)

**Suffixes disponibles :**
- **"ev-justify--center"** : Applique le style `justify-content: center;`
> Les éléments sont groupés au centre du container.

**Exemple :**

```html
<div class="ev-content ev-justify--center">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-justify--center content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

- **"ev-justify--start"** : Applique le style `justify-content: flex-start;`

> Les éléments sont groupés au "début" du container. À gauche pour un container en direction "row", ou en haut pour un container en direction "column".

**Exemple :**

```html
<div class="ev-content ev-justify--start">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-justify--start content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

- **"ev-justify--end"** : Applique le style `justify-content: flex-end;`

> Les éléments sont groupés à la "fin" du container. À droite pour un container en direction "row", ou en bas pour un container en direction "column".

**Exemple :**

```html
<div class="ev-content ev-justify--end">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-justify--end content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

- **"ev-justify--between"** : Applique le style `justify-content: space-between;`

> Les éléments sont répartis équitablement. Le bord du premier est aligné sur le "début" du conteneur et la fin du dernier est alignée sur la "fin" du container.

**Exemple :**

```html
<div class="ev-content ev-justify--between">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-justify--between content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

- **"ev-justify--around"** : Applique le style `justify-content: space-around;`

> Les éléments sont répartis équitablement. À chaque extrémité, l'espace entre le bord du container et le premier/dernier élément équivaut à la moitié de l'espace appliqué entre chaque élément.

**Exemple :**

```html
<div class="ev-content ev-justify--around">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-justify--around content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

- **"ev-justify--evenly"** : Applique le style `justify-content: space-evenly;`

> Les éléments sont répartis équitablement. Tous les éléments sont séparés par le même espace.

**Exemple :**

```html
<div class="ev-content ev-justify--evenly">
    <div>Élement 1</div>
    <div>Élement 2</div>
    <div>Élement 3</div>
</div>
```
<div class="ev-content ev-justify--evenly content">
    <div class="element">Élement 1</div>
    <div class="element">Élement 2</div>
    <div class="element">Élement 3</div>
</div>

<br/>

### "ev-align"

Permet de définir la manière dont sont répartis les éléments sur l'axe secondaire (cross-axis) d'un 'Container Flex'.
Classe à utiliser avec suffixe de type `--suffix` :

[**> Guide flexbox (axis et bases)**](https://la-cascade.io/flexbox-guide-complet/#lesbases)
<br/>
[**> Guide flexbox (justify-content)**](https://la-cascade.io/flexbox-guide-complet/#alignitems)

**Suffixes disponibles :**
- **"ev-align--center"** : Applique le style `align-items: center;`

> Les éléments sont groupés au centre du container.

**Exemple :**

```html
<div class="ev-element__wrapper ev-align--center">
    <div class="ev-element">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3</div>
</div>
```
<div class="ev-content content example__flex-align">
    <div class="ev-element__wrapper ev-align--center">
        <div class="ev-element ev-element--tier element">Élement 1</div>
        <div class="ev-element ev-element--tier element">Élement 2</div>
        <div class="ev-element ev-element--tier element">Élement 3</div>
    </div>
</div>

<br/>

- **"ev-align--start"** : Applique le style `align-items: flex-start;`

> Les éléments sont groupés au "début" du container. En haut pour un container en direction "row", ou à gauche pour une container en direction "column".

**Exemple :**

```html
<div class="ev-element__wrapper ev-align--start">
    <div class="ev-element">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3</div>
</div>
```
<div class="ev-content content example__flex-align">
    <div class="ev-element__wrapper ev-align--start">
        <div class="ev-element ev-element--tier element">Élement 1</div>
        <div class="ev-element ev-element--tier element">Élement 2</div>
        <div class="ev-element ev-element--tier element">Élement 3</div>
    </div>
</div>

<br/>

- **"ev-align--end"** : Applique le style `align-items: flex-end;`

> Les éléments sont groupés à la "fin" du container. En bas pour un container en direction "row", ou à droite pour une container en direction "column".

**Exemple :**

```html
<div class="ev-element__wrapper ev-align--end">
    <div class="ev-element">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3</div>
</div>
```
<div class="ev-content content example__flex-align">
    <div class="ev-element__wrapper ev-align--end">
        <div class="ev-element ev-element--tier element">Élement 1</div>
        <div class="ev-element ev-element--tier element">Élement 2</div>
        <div class="ev-element ev-element--tier element">Élement 3</div>
    </div>
</div>

<br/>

- **"ev-align--stretch"** : Applique le style `align-items: stretch;`

> Les éléments sont répartis équitablement et les éléments dimensionnés avec 'auto' sont étirés afin de remplir le conteneur.

**Exemple :**

```html
<div class="ev-element__wrapper ev-align--stretch">
    <div class="ev-element">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3<br/>+<br/>Texte voué à occuper l'espace et sans lequel cette exemple serait tout de suite beaucoup moins parlant...</div>
</div>
```
<div class="ev-element__wrapper ev-align--stretch">
    <div class="ev-element ev-element--tier element">Élement 1</div>
    <div class="ev-element ev-element--tier element">Élement 2</div>
    <div class="ev-element ev-element--tier element">Élement 3<br/>+<br/>Texte voué à occuper l'espace et sans lequel cette exemple serait tout de suite beaucoup moins parlant...</div>
</div>

<br/>

- **"ev-align--baseline"** : Applique le style `align-items: baseline;`

> Tous les éléments flexibles sont alignés afin que leurs différentes lignes de base soient alignées. L'élément pour lequel on a la plus grande distance entre la marge et la ligne de base est aligné sur le bord de la ligne courante.

**Exemple :**

```html
<div class="ev-element__wrapper ev-align--baseline">
    <div class="ev-element">Élement 1</div>
    <div class="ev-element">Élement 2</div>
    <div class="ev-element">Élement 3</div>
    <div class="ev-element">Élement 4</div>
</div>
```
<div class="ev-content content example__flex-align example__flex-align-baseline">
    <div class="baseline"></div>
    <div class="ev-element__wrapper ev-align--baseline">
        <div class="ev-element ev-element--quarter element">Élement 1</div>
        <div class="ev-element ev-element--quarter element">Élement 2</div>
        <div class="ev-element ev-element--quarter element">Élement 3</div>
        <div class="ev-element ev-element--quarter element">Élement 4</div>
    </div>
</div>

<br/>

### "ev-order"

Permet de définir l'ordre d'affichage des éléments dans un 'Container Flex'.
> - Classe à utiliser avec suffixe de type `--{valeur}`.
> - Les "valeurs" sont à remplacer par une valeur numérique comprise entre les valeurs des variables `$properties-min-order-value` et `$properties-min-order-value`.

```html
<div class="ev-content">
    <div class="ev-element ev-order--3">Élément 1</div>
    <div class="ev-element ev-order--1">Élément 2</div>
    <div class="ev-element ev-order--4">Élément 3</div>
    <div class="ev-element ev-order--2">Élément 4</div>
</div>
```
<div class="ev-content content">
    <div class="ev-element ev-element--quarter ev-order--3 element">Élément 1</div>
    <div class="ev-element ev-element--quarter ev-order--1 element">Élément 2</div>
    <div class="ev-element ev-element--quarter ev-order--4 element">Élément 3</div>
    <div class="ev-element ev-element--quarter ev-order--2 element">Élément 4</div>
</div>

<br/>

### "ev-wrap" & "ev-nowrap"

Permet de définir le comportement des éléments au sein d'un 'Container Flex', lorsque ces derniers occupent plus que la largeur disponible.

- **"ev-wrap"** : Applique le style `flex-wrap: wrap;`
> Les éléments à l'intérieur d'un 'Container Flex' passent à la ligne une fois la largeur maximum du container atteinte.

**Exemple :**
```html
<div class="ev-element__wrapper ev-wrap">
    <div class="ev-element">Élément 1</div>
    <div class="ev-element">Élément 2</div>
    <div class="ev-element">Élément 3</div>
    <div class="ev-element">Élément 4</div>
</div>
```
<div class="ev-element__wrapper ev-wrap content example--flex-wrap">
    <div class="ev-element element">Élément 1</div>
    <div class="ev-element element">Élément 2</div>
    <div class="ev-element element">Élément 3</div>
    <div class="ev-element element">Élément 4</div>
</div>

<br/>

- **"ev-nowrap"** : Applique le style `flex-wrap: nowrap;`
> Les éléments ne passent pas à la ligne et débordent du container.

**Exemple :**
```html
<div class="ev-element__wrapper ev-nowrap">
    <div class="ev-element">Élément 1</div>
    <div class="ev-element">Élément 2</div>
    <div class="ev-element">Élément 3</div>
    <div class="ev-element">Élément 4</div>
</div>
```
<div class="ev-element__wrapper ev-nowrap content example--flex-nowrap">
    <div class="ev-element element">Élément 1</div>
    <div class="ev-element element">Élément 2</div>
    <div class="ev-element element">Élément 3</div>
    <div class="ev-element element">Élément 4</div>
</div>

<br/>

### "ev-margin" & "ev-padding"

Permet d'ajouter des 'margin' et des 'padding' à un élément.
> - Classe à utiliser avec suffixe de type `--{side}-{valeur}`.
> - Les "sides" sont explicités dans la liste ci-dessous.
> - Les "valeurs" sont à remplacer soit par une valeur spécifiée dans la variable `$properties-size-values`, soit par une valeur numérique comprise entre les valeurs des variables `$properties-min-rem-value` et `$properties-min-rem-value` (unité "rem").

 entre les valeurs des variables `$properties-min-rem-value` et `$properties-min-rem-value`**Liste des "sides" (unité "rem)" disponibles :**
- `--all` : Applique un margin ou un padding à tous les côtés.
- `--top` : Applique un margin ou un padding en haut.
- `--right` : Applique un margin ou un padding à droite.
- `--bot` : Applique un margin ou un padding en bas.
- `--left` : Applique un margin ou un padding à gauche.
- `--tb` : Applique un margin ou un padding en haut et en bas.
- `--lr` : Applique un margin ou un padding à gauche et à droite.

<br/>

### "ev-height" & "ev-width"

Permet de définir le 'height' ou le 'width' d'un élément.
> Classes à utiliser avec un suffixe de type `--{valeur}`, à remplacer soit par l'une des valeurs spécifiées dans la variable `$properties-size-values`, soit par une valeur numérique comprise entre les valeurs des variables `$properties-min-rem-value` et `$properties-min-rem-value` (unité "rem").

**Exemple :**
- **"ev-height--big"** : Applique le style `height: 2rem;`

<br/>

### "ev-min-height" & "ev-min-width"

Permet de définir le 'min-height' ou le 'min-width' d'un élément.
> Classes à utiliser avec un suffixe de type `--{valeur}`, à remplacer soit par l'une des valeurs spécifiées dans la variable `$properties-size-values`, soit par une valeur numérique comprise entre les valeurs des variables `$properties-min-rem-value` et `$properties-min-rem-value` (unité "rem").

**Exemple :**
- **"ev-min-width--big"** : Applique le style `min-width: 2rem;`

<br/>

### "ev-max-height" & "ev-max-width"

Permet de définir le 'max-height' ou le 'max-width' d'un élément.
> Classes à utiliser avec un suffixe de type `--{valeur}`, à remplacer soit par l'une des valeurs spécifiées dans la variable `$properties-size-values`, soit par une valeur numérique comprise entre les valeurs des variables `$properties-min-rem-value` et `$properties-min-rem-value` (unité "rem").

**Exemple :**
- **"ev-max-width--big"** : Applique le style `max-width: 2rem;`

## <span style="color: #3fc8ff;">■</span> Responsive

- **Layout 12 colonnes :**

Une série de classes a été ajoutée afin de définir le comportement des éléments en fonction des différentes tailles d'écrans (media-queries).

**Syntaxe :**

Ces classes se composent d'un Préfixe `ev-`, suivi d'un Block `small` ou `medium` correspondant à la taille en-dessous de laquelle appliquer les propriétés, suivis d'un Complément `__{nature}` destiné à définir la propriété à appliquer.
<br/>
Selon les cas de figures, un Suffixe de type `--{suffixe}` viendra compléter cet ensemble afin d'y ajouter un niveau de spécificité.
> Les tailles d'affichages "small" et "medium" sont définies par les variables `$breakpoint-medium` et `$breakpoint-small` de "_variable.scss".

**Exemple de syntaxe :**
```html
<div class="ev-medium__justify--center"></div>
```
> Dans ce cas de figure, pour un affichage "medium" et inférieur, le contenu de la `<div>` hérite de la propriété `justify-content: center;`.

<br/>

### Responsive du Layout 12 colonnes

Le système de Layout 12 colonnes inclue une série de classes spécifiques, afin d'adapter, au besoin, la taille des éléments aux différents affichages.
> Par défaut, les éléments `"ev-element"` passent à la ligne et utilisent 100% de la largeur à partir d'une taille d'affichage égale ou inférieure à "small".

<br/>

- **"ev-medium__element" & "ev-small__element" :**

Afin de les rendres opérantes, ces classes doivent être complétées d'un suffixe spécifiant la taille que l'on souhaite donner à l'élément.

**Exemple :**
```html
<div class="ev-element ev-small__element--half"></div>
```
> Pour les affichages de taille "small" et inférieure, cet élément occupera la moitié de la largeur disponible.

**Suffixes disponibles :**
- `--half` : En-dessous de la résolution définie, l'élément occupera un **1/2 de la largeur disponible**.
- `--quarter` : En-dessous de la résolution définie, l'élément occupera un **1/4 de la largeur disponible**.
- `--quarter-3` : En-dessous de la résolution définie, l'élément occupera les **3/4 de la largeur disponible**.
- `--tier` : En-dessous de la résolution définie, l'élément occupera un **1/3 de la largeur disponible**.
- `--tier-2` : En-dessous de la résolution définie, l'élément occupera les **2/3 de la largeur disponible**.
- `--sixth` : En-dessous de la résolution définie, l'élément occupera un **1/6 de la largeur disponible**.
- `--sixth-5` : En-dessous de la résolution définie, l'élément occupera les **5/6 de la largeur disponible**.

<br/>

### Responsive des propriétés 'Flex' générales

- **"ev-small__direction" :**

Permet de définir la direction de l'axe principal (main-axis) d'un 'Container Flex' , en fonction des différentes tailles d'affichage (voir fonctionnement "ev-direction").
> Existe en variante "medium".

**Exemple :**
```html
<div class="ev-element__wrapper ev-small__direction--column"></div>
```
> Pour les affichages de taille "small" et inférieure, l'axe principal (main-axis) de ce 'Container Flex' devient vertical.

<br/>

- **"ev-small__justify" & "ev-small__align" :**

Permet de définir le positionnement sur l'axe principal des éléments se trouvant à l'intérieur d'un 'Container Flex', en fonction des différentes tailles d'affichage (voir fonctionnement "ev-justify" et "ev-align").
> Existe en variante "medium".

**Exemple :**
```html
<div class="ev-element ev-small__justify--center"></div>
```
> Pour les affichages de taille "small" et inférieure, le contenu de cet élément sera justifié au centre.

<br/>

- **"ev-small__wrap" & "ev-small__nowrap" :**

Permet d'autoriser, ou non, le passage à la ligne des éléments se trouvant à l'intérieur d'un 'Container Flex', en fonction des différentes tailles d'affichage (voir "ev-wrap" et "ev-nowrap").
> Existe en variante "medium".

**Exemple :**
```html
<div class="ev-element__wrapper ev-small__nowrap"></div>
```
> Pour les affichages de taille "small" et inférieure, les éléments présents à l'intérieur de ce 'Container Flex' ne passent pas à la ligne lorsqu'ils dépassent la largeur totale disponible.
